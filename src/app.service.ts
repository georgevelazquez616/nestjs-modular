import { Injectable, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ConfigType } from '@nestjs/config';
import config  from './config';

@Injectable()
export class AppService {

  constructor(
  //  @Inject('API_KEY') private apiKey: any[],
   // @Inject('TASKS') private tasks: any[],
    @Inject(config.KEY) private configService: ConfigType<typeof config>,
   // private config: ConfigService,
  ){}
  getHello(): string {
    const apiKey = this.configService.apiKey;
    const name = this.configService.database.name;
    return `Hello word! ${apiKey} ${name}`;
}
}
